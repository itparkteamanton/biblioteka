package server;

import java.io.*;
import java.net.Socket;
import java.nio.charset.StandardCharsets;
import java.util.Scanner;

public class ClientHandler implements Runnable {
    private Socket socket;
    public PrintWriter out;
    private Scanner in;
    private static int clientsCount = 0;
    private String clientName;

    public ClientHandler(Socket s) throws FileNotFoundException {
        try {
            this.socket = s;
            out = new PrintWriter(s.getOutputStream());
            in = new Scanner(s.getInputStream());
            clientsCount++;
            clientName = "Client #" + clientsCount;
        } catch (IOException e) {
        }
    }

    @Override
    public void run() {
        out.write("Hello my dear READER!!!\n");
        out.write("If you want read what some BOOK, write 'library' \n");
        out.flush();
        while (true) {
            if (in.hasNext()) {
                String message = in.nextLine();
                System.out.println(clientName + ": " + message);
                out.println("ECHO: " + message);
                out.flush();
                if (message.equalsIgnoreCase("END")) break;
                if (message.equalsIgnoreCase("library"))
                    try {
                        libraryBook();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
            }
        }
        try {
            System.out.println("Client disconnected");
            socket.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void libraryBook() throws IOException {
        out.println("Выберите раздел:");
        out.println("cyberpank");
        out.println("detective");
        out.println("fantasy");
        out.println("history");
        out.println("humor");
        out.flush();

        if (in.hasNext()) {
            String message = in.nextLine();
            System.out.println(clientName + ": " + message);
            out.println("ECHO: " + message);
            out.flush();
            if (message.equals("cyberpank")) cyberpank();
            if (message.equals("detective")) detective();
            if (message.equals("fantasy")) fantasy();
            if (message.equals("history")) history();
            if (message.equals("humor")) humor();
            else libraryBook();
        }
    }

    private void humor() throws IOException {
        out.println("Выберите книгу: ");
        out.println("every day apocalypsis");
        out.println("fourth quarter");
        out.flush();
        if (in.hasNext()) {
            String message = in.nextLine();
            System.out.println(clientName + ": " + message);
            out.println("ECHO: " + message);
            out.flush();
            if (message.equals("every day apocalypsis"))
                HandlerTxtFiles("src/Library/library/humor/every day apocalypsis.txt");
            if (message.equals("fourth quarter"))
                HandlerTxtFiles("src/Library/library/humor/fourth quarter.txt");
            else fantasy();
        }
    }

    private void history() throws IOException {
        out.println("Выберите книгу: ");
        out.println("sister");
        out.println("song of valkyries");
        out.println("varangian");
        out.println("white dragon temple");
        out.flush();
        if (in.hasNext()) {
            String message = in.nextLine();
            System.out.println(clientName + ": " + message);
            out.println("ECHO: " + message);
            out.flush();
            if (message.equals("sister"))
                HandlerTxtFiles("src/Library/library/history/sister.txt");
            if (message.equals("song of valkyries"))
                HandlerTxtFiles("src/Library/library/history/song of valkyries.txt");
            if (message.equals("varangian"))
                HandlerTxtFiles("src/Library/library/history/varangian.txt");
            if (message.equals("white dragon temple"))
                HandlerTxtFiles("src/Library/library/history/white dragon temple.txt");
            else fantasy();
        }
    }

    private void fantasy() throws IOException {
        out.println("Выберите книгу: ");
        out.println("autum");
        out.println("dragon hunting");
        out.println("vedmyak");
        out.flush();
        if (in.hasNext()) {
            String message = in.nextLine();
            System.out.println(clientName + ": " + message);
            out.println("ECHO: " + message);
            out.flush();
            if (message.equals("autum"))
                HandlerTxtFiles("src/Library/library/fantasy/autum.txt");
            if (message.equals("dragon hunting"))
                HandlerTxtFiles("src/Library/library/fantasy/dragon hunting.txt");
            if (message.equals("vedmyak"))
                HandlerTxtFiles("src/Library/library/fantasy/vedmyak.txt");
            else fantasy();
        }
    }

    private void detective() throws IOException {
        out.println("Выберите книгу: ");
        out.println("simple things");
        out.println("smell of magic");
        out.flush();
        if (in.hasNext()) {
            String message = in.nextLine();
            System.out.println(clientName + ": " + message);
            out.println("ECHO: " + message);
            out.flush();
            if (message.equals("simple things"))
                HandlerTxtFiles("src/Library/library/detective/simple things.txt");
            if (message.equals("smell of magic"))
                HandlerTxtFiles("src/Library/library/detective/smell of magic.txt");
            else detective();

        }
    }

    private void cyberpank() throws IOException {
        out.println("Выберите книгу: ");
        out.println("Citadel");
        out.println("Great-worlds");
        out.println("Hole");
        out.flush();
        if (in.hasNext()) {
            String message = in.nextLine();
            System.out.println(clientName + ": " + message);
            out.println("ECHO: " + message);
            out.flush();
            if (message.equals("citadel"))
                HandlerTxtFiles("src/Library/library/cyberpank/citadel.txt");
            if (message.equals("great-worlds"))
                HandlerTxtFiles("src/Library/library/cyberpank/great-worlds.txt");
            if (message.equals("hole"))
                HandlerTxtFiles("src/Library/library/cyberpank/hole.txt");
            else cyberpank();
        }
    }

    private void HandlerTxtFiles(String FILE_NAME) throws IOException {
        BufferedReader reader = new BufferedReader(
                new InputStreamReader(
                        new FileInputStream(FILE_NAME), StandardCharsets.UTF_8));
        String line;
        int count = 0;
        int secondCount = 50;

        while (((line = reader.readLine()) != null) && (count < secondCount)) {
            out.write(line + "\n");
            out.flush();
            count++;
            if (count == secondCount) {
                secondCount = secondCount + 50;
                out.write("Если вы хотите загрузить следующие 50 строк, введите next\n");
                out.write("Если вы хотите выйти, введите end\n");
                out.flush();
                nextOrEnd();
            }
        }
    }

    private void nextOrEnd() throws IOException {
        if (in.hasNext()) {
            String message = in.nextLine();
            System.out.println(clientName + ": " + message);
            out.println("ECHO: " + message);
            out.flush();
            if (message.equals("next")) return;
            else if (message.equalsIgnoreCase("end")) {
                socket.close();
            } else {
                out.write("Если вы хотите загрузить следующие 50 строк, введите next\n");
                out.write("Если вы хотите выйти, введите end\n");
                nextOrEnd();
            }
        }
    }
}

